import React from 'react';
import Calculator from './components/Calculator';

const App: React.FC = () => {
  return (
    <div className="wrapper">
      <h1>Калькулятор расчёта выигрыша</h1>
      <div className="form-wrapper">
        <h2 className="title-form">Сколько можно выиграть?</h2>
        <Calculator/>
        <hr className="dashed" />
        <ul className="list-dots">
          <li>Lorem Ipsum is simply dummy text of the printing  and typesetting industry. </li>
          <li>Lorem Ipsum is simply dummy text of the printing  and typesetting industry. </li>
        </ul>
      </div>
    </div >
  );
}

export default App;
