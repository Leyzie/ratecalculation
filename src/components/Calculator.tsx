import React, { Component } from 'react'
import ErrrosBox from '../containers/ErrrosBox'


interface IState {
    rate: number;
    coefficient:  number;
    pay: number;
    summ: number;

    formErrors: IValide;

    showsumm: boolean;
}

interface IValide {
    errorRate: string;
    errorCoefficient: string;
}

class Calculator extends Component<{},IState> {
    state = {
        rate: 0,
        coefficient: 0,
        pay: 0,
        summ: 0,
        formErrors: {
            errorRate: '',
            errorCoefficient: ''
        },
        showsumm: false
    }

    rateChenge = (e: React.ChangeEvent<HTMLInputElement>) => {
        let numberTransform: number = +e.currentTarget.value
        this.setState({rate: numberTransform})
    }

    coefficientChenge = (e: React.ChangeEvent<HTMLInputElement>) => {
        let numberTransform: number = +e.currentTarget.value
        this.setState({coefficient: numberTransform})
    }

    onSubmit = (e: React.SyntheticEvent<HTMLButtonElement>) => {
        e.preventDefault()
        this.validateForm()
    }

    validateForm() {
        let fieldValidationErrors = this.state.formErrors;
        let rateVal = this.state.rate
        let coefficientVal = this.state.coefficient
        if(rateVal < 10){
            fieldValidationErrors.errorRate = 'Ставка должна быть больше 10 руб.'
            this.setState({showsumm: false}) 
        }else{
            fieldValidationErrors.errorRate = '' 
        }
        

        if(coefficientVal < 1){
            fieldValidationErrors.errorCoefficient = 'Коэффициент должен быть больше 1'
            this.setState({showsumm: false}) 
        }else{
            fieldValidationErrors.errorCoefficient = ''
        }

        this.setState({
            formErrors: fieldValidationErrors
        })

        if(
            fieldValidationErrors.errorCoefficient === '' &&
            fieldValidationErrors.errorRate === ''
        ){ 
            let calcPay = rateVal * coefficientVal
            this.setState({
                showsumm: true,
                pay: calcPay,
                summ: calcPay - rateVal
            })
        } else{
            this.setState({showsumm: false}) 
        }
        // console.log(this.state.formErrors)
        // console.log(this.state.rate)
        // console.log(this.state.coefficient)
        // console.log(this.state.showsumm)
    }

    render() {
        const {showsumm, pay, summ, formErrors} = this.state
        return (
            <form className="form_calc">
                <div className="from-group">
                    <label htmlFor="rate">Если поставить, руб</label>
                    <input
                        type="number"
                        defaultValue=""
                        id="rate"
                        min="10"
                        name="rate"
                        className="not-control"
                        placeholder="от 10 и больше"
                        onChange={this.rateChenge}
                    />
                    <ErrrosBox text={formErrors.errorRate}/>
                </div>
                <div className="from-group">
                    <label htmlFor="coefficient">На коэффициент</label>
                    <input
                        type="number"
                        defaultValue=""
                        id="coefficient"
                        min="1"
                        name="coefficient"
                        className="not-control"
                        placeholder="от 1 и больше"
                        onChange={this.coefficientChenge}
                    />
                    <ErrrosBox text={formErrors.errorCoefficient}/>
                </div>
                {showsumm&& 
                    <>
                        <hr className="dashed" />
                        <div className="from-group">
                            <label  htmlFor="">Выплата</label>
                            <input type="text" readOnly  value={`${pay} ₽`} />
                        </div>
                        <div className="from-group">
                            <label htmlFor="">Сумма выигрыша</label>
                            <input type="text" className="green" readOnly  value={`${summ} ₽`}/>
                        </div>
                    </>
                }
                <button className={`btn  ${showsumm? 'btn-read' : 'btn-blue'} ml-auto max-width-394`} onClick={this.onSubmit}>
                    {showsumm? 'Расчитать' : 'Посчитать'}
                </button>
            </form>
        )
    }
}

export default Calculator