import React from 'react'

interface Props {
    text: string
}

const ErrrosBox: React.FC<Props> = props => {
    return (
        <>
            {props.text.length > 0 &&
               <span className="error">{props.text}</span>
            }
        </>
    )
}

export default ErrrosBox
